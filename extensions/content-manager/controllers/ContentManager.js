'use strict';

const _ = require('lodash');
const { contentTypes: contentTypesUtils } = require('strapi-utils');

const parseMultipartBody = require('../utils/parse-multipart');
// const {
//   validateGenerateUIDInput,
//   validateCheckUIDAvailabilityInput,
//   validateUIDField,
// } = require('./validation');

const ACTIONS = {
    read: 'plugins::content-manager.explorer.read',
    create: 'plugins::content-manager.explorer.create',
    edit: 'plugins::content-manager.explorer.update',
    delete: 'plugins::content-manager.explorer.delete',
    publish: 'plugins::content-manager.explorer.publish',
  };
  

module.exports = {
      /**
   * Creates an entity of a content type
   */
  async create(ctx) {

  
    console.log('strapi create');



    const {
      state: { userAbility, user },
      params: { model },
      request: { body },
    } = ctx;
    const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;
    const modelDef = strapi.getModel(model);

    const pm = strapi.admin.services.permission.createPermissionsManager(
      userAbility,
      ACTIONS.create,
      model
    );

    if (!pm.isAllowed) {
      throw strapi.errors.forbidden();
    }

    const sanitize = e => pm.pickPermittedFieldsOf(e, { subject: model });

    const { data, files } = ctx.is('multipart') ? parseMultipartBody(ctx) : { data: body };

    const writableData = _.omit(data, contentTypesUtils.getNonWritableAttributes(modelDef));

    await strapi.entityValidator.validateEntityCreation(modelDef, writableData, { isDraft: true });
    const isDraft = contentTypesUtils.hasDraftAndPublish(modelDef);
    await strapi.entityValidator.validateEntityUpdate(modelDef, writableData, { isDraft });

    try {
      const result = await contentManagerService.create(
        {
          data: {
            ...sanitize(writableData),
            // [CREATED_BY_ATTRIBUTE]: user.id,
            // [UPDATED_BY_ATTRIBUTE]: user.id,
          },
          files,
        },
        { model }
      );

      ctx.body = pm.sanitize(result, { action: ACTIONS.read });

      await strapi.telemetry.send('didCreateFirstContentTypeEntry', { model });
    } catch (error) {
      strapi.log.error(error);
      ctx.badRequest(null, [
        {
          messages: [{ id: error.message, message: error.message, field: error.field }],
          errors: _.get(error, 'data.errors'),
        },
      ]);
    }
  },
}